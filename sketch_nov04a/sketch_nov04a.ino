
/*
  Stepper Motor Control - one revolution
  This program drives a unipolar or bipolar stepper motor.
  The motor is attached to digital pins 8 - 11 of the Arduino.
  The motor should revolve one revolution in one direction, then
  one revolution in the other direction.
  Created 11 Mar. 2007
  Modified 30 Nov. 2009
  by Tom Igoe
*/

#include <ArduinoJson.h>
#include <Stepper.h>
long a = 1600;
const int stepsPerRevolution = 400;  // change this to fit the number of steps per revolution
// for your motor

// initialize the stepper library on pins 8 through 11:
Stepper myStepper(stepsPerRevolution, 10, 11);
Stepper myStepper1(stepsPerRevolution, 6, 7);
DynamicJsonBuffer jsonBuffer;
JsonObject& root1 = jsonBuffer.createObject();
String receiveBuff = "";
String  aaa = "";
int  countX = 0;
int  countY = 0;

boolean isForever = false;
int port = 0;  //port is   x,y,z status    0 is close   1(top) 2(left) 3(right) 4(bottom)
void setup() {

  // set the speed at 60 rpm:
  myStepper.setSpeed(1000);
  myStepper1.setSpeed(1000);
  // initialize the serial port:
  Serial.begin(9600);
  Serial1.begin(9600);
}

void loop() {
  while (Serial1.available() > 0)
  {
    char str = Serial1.read();
    receiveBuff += str;
    if (str == '\n')
    {
      analysisJson(receiveBuff.trim());
    }

  }


  // Long press
  run() ;
}

void  analysisJson(String json) {

  JsonObject& root = jsonBuffer.parseObject(json);
  Serial.println(receiveBuff);

  if (!root.success()) {
    Serial1.println("failed");
    Serial.println("failed");
    return;
  }
  int   orientation = root["d"];
  String  menu = root["m"];
  receiveBuff = "";
  Serial.println("1");
  if (menu == "start" ) {
    isForever = true;
    port = orientation
  } else if (menu == "click") {
    port = orientation;
  }
  else if (menu == "get"  ) {
    root["xValue"] = countX;
    root["yValue"] = countY;

  }
  else if("over"==menu){
    
    //完成施教。。。
    }
    .
     else if("automatic"==menu){
    
    //自动执行。。。
    }
    else if("clear"==menu){
    
    //清空。。。
    }
    
  else {
    isForever = false;
    port = 0
           return;
  }

}

void  run() {
  if (isForever) {
    .policyClick ();
  }

}



void   policyClick () {
  if (port == 1) {
    myStepper1.step(1);
    delay(1);
    countY += countY;
  }
  else if (port == 4)
  {
    myStepper1.step(-1);
    delay(1);
    countY -= countY;
  }  else if (port == 2)
  {
    myStepper.step(-1);
    delay(1);
    countX -= countX;
  }  else if (port == 3)
  {
    myStepper.step(1);
    delay(1);
    countX += countX;
  }
}





