package com.example.fail5.link2arduino.bean;

public class Point {

    int xValue;
    int yValue;

    private String createDate;


    public Point(int xValue, int yValue) {
        this.xValue = xValue;
        this.yValue = yValue;
    }

    public Point(int xValue, int yValue, String createDate) {
        this.xValue = xValue;
        this.yValue = yValue;
        this.createDate = createDate;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getxValue() {
        return xValue;
    }

    public void setxValue(int xValue) {
        this.xValue = xValue;
    }

    public int getyValue() {
        return yValue;
    }

    public void setyValue(int yValue) {
        this.yValue = yValue;
    }

    @Override
    public String toString() {
        return "Point{" +
                "xValue=" + xValue +
                ", yValue=" + yValue +
                '}';
    }
}
