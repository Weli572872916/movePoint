package com.example.fail5.link2arduino.adapter;

import android.os.Build;
import android.support.annotation.Nullable;
import android.text.Html;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.fail5.link2arduino.R;
import com.example.fail5.link2arduino.bean.Point;

import java.util.List;

/**
 * @author Weli
 * @time 2017-11-17  11:49
 * @describe
 */
public class PointAdapter extends BaseQuickAdapter<Point, BaseViewHolder> {
    public PointAdapter(int layoutResId, @Nullable List<Point> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper,Point item) {

            helper.setText(R.id.tv_item_x,item.getxValue());
            helper.setText(R.id.tv_item_y,item.getyValue());
        }




}
