package com.example.fail5.link2arduino;

/**
 * @ProjectName: link2Arduino
 * @Package: com.example.fail5.link2arduino
 * @ClassName: Send
 * @Description: java类作用描述
 * @Author: WeLi
 * @CreateDate: 2018/11/2 16:25
 * @UpdateUser: 更新者
 * @UpdateDate: 2018/11/2 16:25
 * @UpdateRemark: 更新说明
 * @Version: 1.0
 */
public class Send {
    String menu;
    int direction;

    public Send(String menu, int direction) {
        this.menu = menu;
        this.direction = direction;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "Send{" +
                "menu='" + menu + '\'' +
                ", direction=" + direction +
                '}';
    }
}
