package com.example.a3axismotion.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import org.jetbrains.annotations.Nullable;

public class Rectangle extends View {


    /**
     * 棋盘的宽度和高度
     */
    private int mPanelWidth;
    /**
     * 每一行的高度
     */
    private float mLineHeight;

    private int MAX_LINE = 2;
    private  final  int   INTERVAL_VALUE=1;
    /**
     * 画笔
     */
    private Paint mPaint = new Paint();
    int startPointX = 0;
    int startPointY = 0;

    private int maxX = 0;
    private int minX = 0;
    private Path path;

    int x = 0;
    int y = 0;

    /**
     * 白棋子和黑棋子的图片
     */
    public Rectangle(Context context) {
        super(context);
    }

    public Rectangle(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        //初始化画笔，获取棋子的图片等
        init();
    }

    private void init() {
        mPaint.setColor(0x88000000);
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        //因为要绘制正方形，所以取宽和高的最小值
        int width = Math.min(widthSize, heightSize);
        //heightMode
        if (widthMode == MeasureSpec.UNSPECIFIED) {
            width = heightSize;

        } else if (heightMode == MeasureSpec.UNSPECIFIED) {
            width = widthSize;

        }
        setMeasuredDimension(width, width);
    }

    /**
     * 当宽高确定后赋值
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mPanelWidth = getHeight();
        Log.d("lattice", "mPanelWidth:" + mPanelWidth);
        //总高度除以行数为每一行的高度
        mLineHeight = h * 1.0f / MAX_LINE;
        Log.d("lattice", "mLineHeight:" + mLineHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //绘制棋盘
        drawBoard(canvas);

    }

    /**
     * @param canvas
     */
    private void drawBoard(Canvas canvas) {
        int w = mPanelWidth;
        float lineHeight = mLineHeight;
        for (int i = 0; i < MAX_LINE; i++) {
            int startX = (int) (lineHeight / 2);
            int endX = (int) (w - lineHeight / 2);
            int y = (int) ((0.5 + i) * lineHeight);
            canvas.drawLine(startX, y, endX, y, mPaint);
            if (i == 0 || i == MAX_LINE - 1) {
                canvas.drawLine(y, startX, y, endX, mPaint);
            }
            startPointX = startX;
            startPointY = startX;
            minX = startX;
            maxX = endX;

        }
        mPaint.setColor(Color.RED);
        path = new Path();

        canvas.drawCircle(startPointX + x, startPointY+y, 10, mPaint);
        canvas.drawPath(path, mPaint);
    }


    public void xChange2X(boolean isXAdd) {
        if (isXAdd) {
            if ((startPointX + x)  == maxX) {
                return;
            }
            x = x + INTERVAL_VALUE;

        } else {
            if ((startPointX + x) - INTERVAL_VALUE < minX) {
                return;
            }
            x = x - INTERVAL_VALUE;

        }

        invalidate();

    }

    public void xChange2Y(boolean isYAdd) {
        if (isYAdd) {
            if ((startPointY + y)  == maxX) {
                return;
            }
            y = y + INTERVAL_VALUE;

        } else {
            if ((startPointY + y) - INTERVAL_VALUE < minX) {
                return;
            }
            y = y - INTERVAL_VALUE;

        }

        invalidate();

    }


}
