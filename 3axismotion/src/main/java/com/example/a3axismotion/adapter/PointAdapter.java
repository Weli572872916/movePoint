package com.example.a3axismotion.adapter;


import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.a3axismotion.R;
import com.example.a3axismotion.bean.Point;

import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * @author Weli
 * @time 2017-11-17  11:49
 * @describe
 */
public class PointAdapter extends BaseQuickAdapter<Point, BaseViewHolder> {
    public PointAdapter(int layoutResId, @Nullable List<Point> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper,Point item) {

            helper.setText(R.id.tv_item_x,item.getxValue());
            helper.setText(R.id.tv_item_y,item.getyValue());
        }




}
